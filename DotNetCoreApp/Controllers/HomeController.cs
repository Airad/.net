using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetCoreApp.Models;
using Microsoft.EntityFrameworkCore;

[ApiController]
[Route("api/")]
public class HomeController : Controller
{
    private readonly DotNetCoreAppContext _context;
    public HomeController(DotNetCoreAppContext context)
    {
        _context = context;
        var driver = new Driver
        {
            Name = "Daria",
            Surname = "Ostapczuk"
        };

        var package = new Package
        {
            Address = "jwefujfbudufhs",
            Quantity = 404,
            Weight = 120
        };

        var packagesDriver = new PackagesDriver
        {
            From = DateTime.Now,
            To = DateTime.Now.AddDays(1),
            PackageId = 1,
            DriverId = 1
        };

        _context.Driver.Add(driver);
        _context.Package.Add(package);
        _context.SaveChanges();
        _context.Add(new PackagesDriver {
                    PackageId=1,
                    //Package=package,
                    DriverId=1,
                    //Driver=driver,
                    From=DateTime.Now,
                    To=DateTime.Now.AddDays(1)
                    });
        _context.SaveChanges();
        _context.PackagesDriver.Add(packagesDriver);
        _context.SaveChanges();
    }

    public void Index()
    {   
    }
}