using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetCoreApp.Models;
using Microsoft.EntityFrameworkCore;

namespace DotNetCoreApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController : Controller
    {
        private readonly DotNetCoreAppContext _context;
        public PackageController(DotNetCoreAppContext context)
        {
            _context=context;
            /* if(_context.Package.Count()==0){
                _context.Add(new Package {Address="Drzewna 15/15 01-517 Warszawa", Quantity=1,Weight=2.3});
                _context.SaveChanges();
            }*/
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Package>>> GetPackages()
        {
            return await _context.Package.Include(x=>x.PackageDrivers).ToListAsync();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Package>> GetPackage(long id){
            var package=await _context.Package.FindAsync(id);

            if(package==null){
                return NotFound();
            }
            return package;
        }
        [HttpPost]
        public async Task<ActionResult<Package>> CreatePackage(Package item){
            _context.Package.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPackage), new{id=item.Id}, item);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> EditPackage(long id, Package item){
            if(id!=item.Id){
                return BadRequest();
            }
            _context.Entry(item).State=EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePackage(long id){
            var item = await _context.Package.FindAsync(id);
            
            if(id!=item.Id){
                return NotFound();
            }
            _context.Package.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}