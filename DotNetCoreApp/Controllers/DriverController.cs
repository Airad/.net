using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetCoreApp.Models;
using Microsoft.EntityFrameworkCore;

namespace DotNetCoreApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverController : Controller
    {
        private readonly DotNetCoreAppContext _context;
        
        public DriverController(DotNetCoreAppContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Driver>>> GetDrivers()
        {
            return await _context.Driver.Include(x=>x.DriverPackages).ToListAsync();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Driver>> GetDriver(long id){
            var driver=await _context.Driver.FindAsync(id);

            if(driver==null){
                return NotFound();
            }
            return driver;
        }
        [HttpPost]
        public async Task<ActionResult<Driver>> PostDriver(Driver item){
            _context.Driver.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetDriver), new{id=item.Id}, item);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDriver(long id, Driver item){
            if(id!=item.Id){
                return BadRequest();
            }
            _context.Entry(item).State=EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDriver(long id){
            var item = await _context.Driver.FindAsync(id);
            
            if(id!=item.Id){
                return NotFound();
            }
            _context.Driver.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}