using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetCoreApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace DotNetCoreApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackagesDriverController : Controller
    {
        
        private readonly DotNetCoreAppContext _context;

        public PackagesDriverController (DotNetCoreAppContext context){
            _context=context;
            
           /*  var package = _context.Package.Find((long)1);
            var driver = _context.Driver.Find((long)1); 
            if(_context.PackagesDriver.Count()==0){
                _context.Add(new PackagesDriver {
                    PackageId=1,
                    //Package=package,
                    DriverId=1,
                    //Driver=driver,
                    From=DateTime.Now,
                    To=DateTime.Now.AddDays(1)
                    });
                _context.SaveChanges();
            }*/
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PackagesDriver>>> GetPackagesDrivers(){
            return await _context.PackagesDriver.Include(x=> x.Package).Include(x=>x.Driver).ToListAsync();
        } 
        [HttpGet("{id}")]
        public async Task<ActionResult<PackagesDriver>> GetPackageDriver(long id){
            //var package = _context.Package.FindAsync(package_id);
            //var driver = _context.Driver.FindAsync(driver_id);
            var packagedriver = await _context.PackagesDriver.FindAsync(id);

            if(packagedriver==null){
                return NotFound();
            }
            return packagedriver;
        }
        [HttpPost]
        public async Task<ActionResult<PackagesDriver>> SetDriverToPackage(long driverId, long packageId, DateTime from) {
            var result = await _context.PackagesDriver.FindAsync(packageId);
            DateTime to;

            if (result != null) {
                return BadRequest();
            }

            to = from.AddDays(1);
            var packagedriver = new PackagesDriver 
            {
                PackageId = packageId,
                DriverId = driverId, 
                From = from, 
                To = to
            };
            _context.Add(packagedriver);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPackageDriver), new { packagedriver.Id }, packagedriver);
        }
    }
}