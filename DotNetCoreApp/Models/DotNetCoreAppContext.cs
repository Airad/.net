using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DotNetCoreApp.Models{
    public class DotNetCoreAppContext : DbContext{

         public DotNetCoreAppContext(DbContextOptions<DotNetCoreAppContext> options)
            : base(options)
        {
        }
        public DbSet<Package> Package{get;set;}
        public DbSet<Driver> Driver{get;set;}
        public DbSet<PackagesDriver> PackagesDriver{get;set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Package>().HasKey(x => x.Id);

            modelBuilder.Entity<Driver>().HasKey(x => x.Id);

            modelBuilder.Entity<PackagesDriver>().HasKey(pd => pd.Id);
            modelBuilder.Entity<PackagesDriver>()
                .HasOne<Package>(pd => pd.Package)
                .WithMany(x => x.PackageDrivers)
                .HasForeignKey(pd => pd.PackageId);

            modelBuilder.Entity<PackagesDriver>()
                .HasOne<Driver>(pd => pd.Driver)
                .WithMany(x => x.DriverPackages)
                .HasForeignKey(pd => pd.DriverId);
        }
    }
}