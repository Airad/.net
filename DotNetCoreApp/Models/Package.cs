using System.Collections.Generic;

namespace DotNetCoreApp.Models
{
    public class Package
    {
        public long Id {get; set; }
        public string Address { get; set; }
        public double Weight { get; set; }
        public int Quantity { get; set; }

        public virtual List<PackagesDriver> PackageDrivers { get; set; }

    }
}