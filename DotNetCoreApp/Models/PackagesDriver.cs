using System;
namespace DotNetCoreApp.Models
{
    public class PackagesDriver
    {
        public long Id {get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public long DriverId { get; set; }
        public virtual Driver Driver { get; set; }

        public long PackageId {get; set; }
        public virtual Package Package { get; set; }
    }
}