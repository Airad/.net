using System.Collections.Generic;

namespace DotNetCoreApp.Models{
    public class Driver
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual List<PackagesDriver> DriverPackages{ get; set; }

    }
}